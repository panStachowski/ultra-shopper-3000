<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShoppingListUserTable extends Migration
{

    public function up()
    {
        Schema::create('shopping_list_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer ('user_id')->unsigned ();
            $table->integer ('shopping_list_id')->unsigned ();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('shopping_list_id')->references('id')->on('shopping_lists')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('shopping_list_user');
    }
}
