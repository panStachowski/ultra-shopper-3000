<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStoreToItemsTable extends Migration
{

    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->integer('store_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('store_id');
        });
    }
}
