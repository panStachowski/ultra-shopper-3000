<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemShoppingListTable extends Migration
{
    public function up()
    {
        Schema::create('item_shopping_list', function (Blueprint $table) {
            $table->increments('id');
            $table->integer ('item_id') -> unsigned ();
            $table->integer ('shopping_list_id') -> unsigned ();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('shopping_list_id')->references('id')->on('shopping_lists')->onDelete('cascade');
        });
    }
    public function down()
    {
        Schema::dropIfExists('item_shopping_list');
    }
}
