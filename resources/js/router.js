import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home.vue'
import Lists from './components/shop/lists.vue'
import List from './components/shop/list.vue'
import Categories from './components/shop/Categories.vue'
import NewShop from './components/shop/newShop.vue'


Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {path: '/lists', name: 'lists', component: Lists},
        {path: '/lists/:id', name: 'list', component: List},
        {path: '/categories', name: 'categories', component: Categories},
        {path: '/newshop', name: 'newshop', component: NewShop},


    ],
})
