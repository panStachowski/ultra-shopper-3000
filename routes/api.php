<?php

use Illuminate\Http\Request;
//sori for no swagger, no time :/

//Route::middleware('auth:api')->group(function () {

    //Route::middleware('owner')->group(function () {
        Route::get('lists', 'ListController@index');
        Route::get('list/{id}', 'ListController@show');
        Route::post('list/{id}/grant', 'ListController@grant');
        Route::delete('list/{id}', 'ListController@destroy');
        Route::post('list/{shopping_list_id}/item', 'ItemController@store');
        Route::patch('item/{id}', 'ItemController@update');
        Route::post('item/{id}/status', 'ItemController@status');
        Route::delete('item/{id}', 'ItemController@destroy');
    //});

    Route::post('category', 'CategoryController@store');
    Route::patch('category/{id}', 'CategoryController@update');
    Route::get('categories', 'CategoryController@index');
    Route::delete('category/{id}', 'CategoryController@destroy');

    Route::post('store', 'StoreController@store');
    Route::patch('store/{id}', 'StoreController@update');
    Route::get('stores', 'StoreController@index');
    Route::delete('store/{id}', 'StoreController@destroy');

    Route::post('list', 'ListController@store');

//}); no auth on front

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('register', 'RegisterController@register');
});
