<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingList extends Model
{
    protected $fillable = [
        'name'
    ];

    public function items(){
        return $this->belongsToMany(Item::Class);
    }

    public function users(){
        return $this->belongsToMany('App\User');
    }
}
