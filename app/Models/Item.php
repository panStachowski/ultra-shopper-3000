<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'name', 'amount', 'category_id', 'store_id', 'finished'
    ];
    public function ShoppingLists(){
        return $this->belongsTomany(ShoppingList::Class);
    }
}
