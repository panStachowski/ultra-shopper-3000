<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }
}
