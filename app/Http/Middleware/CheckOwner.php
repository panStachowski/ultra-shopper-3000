<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use App\User;

class CheckOwner
{
    public function handle($request, Closure $next)
    {
        $list = JWTAuth::user()->ShoppingLists->find($request->id);
        if (!$list){
            return response(['messages' => "No permission to perform this action"], 403);
        }

        return $next($request);
    }
}
