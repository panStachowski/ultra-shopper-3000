<?php

namespace App\Http\Services;


use App\Models\ShoppingList;

class BarService
{
    public function check($id)
    {
        $bar = Bar::findOrFail($id);
        $result=(($bar->comments->avg('roll'))+($bar->comments->avg('meat'))+($bar->comments->avg('salad'))+($bar->comments->avg('sauce'))+($bar->comments->avg('price')))/5;
        return $result;

    }

}