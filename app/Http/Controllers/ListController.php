<?php

namespace App\Http\Controllers;

use App\Models\ShoppingList;
use App\User;
use App\Http\Resources\ListResource;
use App\Http\Resources\ShoppingListResource;
use App\Http\Requests\ListRequest;
use JWTAuth;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function index() {
        $list = ShoppingList::get();
        return ListResource::collection($list);
    }

    public function show($id) {
        $list = ShoppingList::findOrFail($id);
        return new ShoppingListResource($list);
    }

    public function store(ListRequest $request)
    {
        $user = JWTAuth::user();
        $list = new ShoppingList([
            'name' => $request->input('name')
        ]);
        $list->save();
        $user->ShoppingLists()->attach($list);
        return new ListResource($list);
    }

    public function grant($id, Request $request) {
        $list = ShoppingList::findOrFail($id);
        $userEmail = $request->input('user_email');
        $user = User::where('email', $userEmail)->firstOrFail();
        $user->ShoppingLists()->attach($list);
        return  ['message' => "User added"];
    }

    public function destroy($id)
    {
        $list = ShoppingList::findOrFail($id);

        if ($list->delete()) {
            return  ['message' => "List removed"];
        }
    }
}
