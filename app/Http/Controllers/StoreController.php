<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Http\Resources\StoreResource;
use App\Http\Requests\StoreRequest;

class StoreController extends Controller
{
    public function store(StoreRequest $request)
    {
        $category = new Store([
            'name' => $request->input('name')
        ]);
        $category->save();
        return new StoreResource($category);
    }

    public function update($id, StoreRequest $request)
    {
        $category = Store::findOrFail($id);
        $category->fill([
            'name' => $request->input('name'),
        ]);
        $category->update();
        return new StoreResource($category);
    }

    public function index() {
        $categories = Store::get();
        return StoreResource::collection($categories);
    }

    public function destroy($id)
    {
        $category = Store::findOrFail($id);

        if ($category->delete()) {
            return  ['message' => "Category deleted"];
        }
    }
}
