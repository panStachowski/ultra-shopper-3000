<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Category;
use App\Models\Store;
use App\Http\Resources\ItemResource;
use App\Http\Requests\ItemRequest;
use App\Models\ShoppingList;

class ItemController extends Controller
{
    public function store(ItemRequest $request, $shopping_list_id)
    {
        //$category = Category::findOrFail($request->input('category_id'));

        $categoryName = $request->input('category_name');
        $storeName = $request->input('store_name');

        $category = Category::where('name', $categoryName)->firstOrFail();
        $store = Store::where('name', $storeName)->firstOrFail();
        $list = ShoppingList::findOrFail($shopping_list_id);

        $item = new Item([
            'name' => $request->input('name'),
            'amount' => $request->input('amount'),
            'category_id' => $category->id,
            'store_id' => $store->id,
        ]);
        $item->save();
        $list->items()->attach($item);

        return new ItemResource($item);
    }

    public function update($id, ItemRequest $request)
    {
        $item = Item::findOrFail($id);
        $item->fill([
            'name' => $request->input('name'),
            'amount' => $request->input('amount'),
        ]);
        $item->update();
        return new ItemResource($item);
    }

    public function status($id)
    {
        $item = Item::findOrFail($id);
        $state = $item->finished;
        $item->update(['finished' => !$state]);
        return new ItemResource($item);
    }

    public function destroy($id)
    {
        $item = Item::findOrFail($id);

        if ($item->delete()) {
            return  ['message' => "Item removed"];
        }
    }
}
