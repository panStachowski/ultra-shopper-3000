<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function store(CategoryRequest $request)
    {
        $category = new Category([
            'name' => $request->input('name')
        ]);
        $category->save();
        return new CategoryResource($category);
    }

    public function update($id, CategoryRequest $request)
    {
        $category = Category::findOrFail($id);
        $category->fill([
            'name' => $request->input('name'),
        ]);
        $category->update();
        return new CategoryResource($category);
    }

    public function index() {
        $categories = Category::get();
        return CategoryResource::collection($categories);
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        if ($category->delete()) {
            return  ['message' => "Category deleted"];
        }
    }
}