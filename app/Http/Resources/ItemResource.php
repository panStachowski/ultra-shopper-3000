<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Category;
use App\Models\Store;

class ItemResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'amount' => $this->amount,
            'finished' => (bool)$this->finished,
            'category' => Category::find($this->category_id, ['name']),
            'store' => Store::find($this->store_id, ['name']),
        ];
    }
}
