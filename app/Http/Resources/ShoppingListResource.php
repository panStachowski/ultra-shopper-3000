<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Item;
use App\Http\Resources\ItemResource;
use App\Http\Resources\UserResource;

class ShoppingListResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'users' => UserResource::collection($this->users),
            'items' => ItemResource::collection($this->items),
        ];
    }
}
